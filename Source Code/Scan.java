import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.net.*;

public class Scan
{
    private Socket target = null;
    private String serverName = null;
    private int port = 0;
    private boolean stop = false;
    private File file = null;
    private PrintWriter fileout = null;
    private String openPorts = "";


    public Scan(String serverName)
    {
        this.serverName = serverName;
    }

    public Scan(Scan other)
    {
        this.target = other.target;
        this.serverName = other.serverName;
        this.port = other.port;
        this.stop = other.stop;
        this.file = other.file;
        this.fileout = other.fileout;
        this.openPorts = other.openPorts;

    }


    public synchronized void portScan(int port)
    {
        try{
            target = new Socket();
            target.connect(new InetSocketAddress(serverName, port), 300);
            System.out.println(port);
            openPorts += (Integer.toString(port) + "\n");
        } catch (ConnectException e) {
        } catch (Exception e) {
            e.getMessage();
        }
    }



    public void writeResult(String openPorts)
    {
        try
        {
            file = new File("scan_results.txt");
            fileout = new PrintWriter(file);
        }
        catch (FileNotFoundException e)
        {
            System.out.println("File not Found");
        }

        fileout.println(openPorts);
        fileout.close();
    }

    public String getOpenPorts()
    {
        return openPorts;
    }

    public void setOpenPorts(String openPorts)
    {
        this.openPorts = openPorts;
    }

    public boolean isStop()
    {
        return stop;
    }

    public void setStop(boolean stop)
    {
        this.stop = stop;
    }

    public String getServerName()
    {
        return serverName;
    }

    public void setServerName(String serverName)
    {
        this.serverName = serverName;
    }

    public int getPort()
    {
        return port;
    }

    public void setPort(int port)
    {
        this.port = port;
    }

    public Socket getTarget()
    {
        return target;
    }

    public void setTarget(Socket target)
    {
        this.target = target;
    }


}
