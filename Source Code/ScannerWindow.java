import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;

public class ScannerWindow extends JFrame implements ActionListener
{
    private final int WINDOW_WIDTH = 350;
    private final int WINDOW_HEIGHT = 250;
    private JLabel msg;
    private JTextField input;
    private JButton scanButton;
    private JButton stopButton;
    private JButton resultsButton;
    private JPanel panel;
    private MyRunnable myRunnable = null;
    private Thread myThread = null;
    private String ports;
    private File file = null;
    private Scanner fin = null;

    public ScannerWindow()
    {
        super("Port Scanner");
        setSize(WINDOW_WIDTH, WINDOW_HEIGHT);
        setVisible(true);
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

        msg = new JLabel("Enter an IP to scan");
        input = new JTextField(14);
        input.addActionListener(this);
        scanButton = new JButton("Scan");
        scanButton.addActionListener(this);
        stopButton = new JButton("Stop");
        stopButton.addActionListener(this);
        resultsButton = new JButton("Results");
        resultsButton.addActionListener(this);
        panel = new JPanel();
        panel.add(msg);
        panel.add(input);
        panel.add(scanButton);
        panel.add(stopButton);
        panel.add(resultsButton);
        add(panel);
        setVisible(true);

    }

    public void resultsWindow()
    {
        try
        {
            file = new File("scan_results.txt");
            fin = new Scanner(file);
        }
        catch (FileNotFoundException e)
        {
            e.getMessage();
        }
        ports = "";
        while(fin.hasNext())
        {
            ports += ", " + fin.nextLine();
        }
        msg = new JLabel(ports);
        panel = new JPanel();
        panel.add(msg);
        add(panel);
        setVisible(true);
    }


    public void actionPerformed(ActionEvent e)
    {
        String text = input.getText();
        myRunnable = new MyRunnable(text);
        myThread = new Thread(myRunnable);


        if (e.getSource() == scanButton)
        {
            myThread.start();
        }

        else if (e.getSource() == stopButton)
        {
            MyRunnable.terminate();
        }

        else if (e.getSource() == resultsButton)
        {
            MyRunnable.terminate();
            resultsWindow();
        }
    }
}
