public class MyRunnable implements Runnable
{
    private Scan myScanner = null;
    private int port = 0;
    private static volatile boolean running = true;

    public MyRunnable(String text)
    {
        myScanner = new Scan(text);
    }

    public static void terminate()
    {
        running = false;
    }

    @Override
    public void run()
    {
        while(running)
        {
            myScanner.portScan(port);
            port++;
        }
        myScanner.writeResult(myScanner.getOpenPorts());
    }
}